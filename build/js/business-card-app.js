// Angular business card test app

angular.module('businessCardApp', [])

.controller('businessCardController', function($scope) {

  // ARRAY FOR BUSINESS CARDS

  $scope.businessCards = [
    {"id": 0, "logoSource": "img/logo-default.png", "yourName": "John Smith", "yourEmail": "johnsmith@gmail.com", "yourPhone": "0418 234 785", "yourWebsite": "http://www.johnsmith.com"},
    {"id": 1, "logoSource": "img/logo-default.png", "yourName": "April June", "yourEmail": "apriljune@gmail.com", "yourPhone": "0468 483 123", "yourWebsite": "http://www.apriljune.com"},
    {"id": 2, "logoSource": "img/logo-default.png", "yourName": "Chris Johns", "yourEmail": "chrisjohns@gmail.com", "yourPhone": "0478 324 907", "yourWebsite": "http://www.chrisjohns.com"},
    {"id": 3, "logoSource": "img/logo-default.png", "yourName": "Ang Jung", "yourEmail": "anjjung@gmail.com", "yourPhone": "0435 422 574", "yourWebsite": "http://www.anjjung.com"},
    {"id": 4, "logoSource": "img/logo-default.png", "yourName": "Sally Winters", "yourEmail": "sallywinters@gmail.com", "yourPhone": "0443 775 363", "yourWebsite": "http://www.sallywinters.com"},
    {"id": 5, "logoSource": "img/logo-default.png", "yourName": "Damien Peters", "yourEmail": "damienpeters@gmail.com", "yourPhone": "0449 235 235", "yourWebsite": "http://www.damienpeters.com"},
    {"id": 6, "logoSource": "img/logo-default.png", "yourName": "Rakesh Chandra", "yourEmail": "rakeshchandra@gmail.com", "yourPhone": "0411 264 235", "yourWebsite": "http://www.rakeshchandra.com"},
    {"id": 7, "logoSource": "img/logo-default.png", "yourName": "Michael Jones", "yourEmail": "michaeljones@gmail.com", "yourPhone": "0499 549 190", "yourWebsite": "http://www.michaeljones.com"}
  ]


  // HIDING SHOWING VIEWING OR EDITING STATE
  
  $scope.isEditing = false;
  $scope.isCreating = false;

  function shouldBeDefault() {
    return !$scope.isCreating && !$scope.isEditing;
  }

  function startCreating() {
    $scope.isEditing = false;
    $scope.isCreating = true;
    //resetCreateForm();
  }

  function cancelCreating() {
    $scope.isCreating = false;
  }

  function startEditing() {
    $scope.isEditing = true;
    $scope.isCreating = false;
  }

  function cancelEditing() {
    $scope.isEditing = false;
    $scope.editedBusinessCard = null;
  }

  function shouldBeCreating() {
    return $scope.isCreating && !$scope.isEditing;
  }

  function shouldBeEditing() {
    return $scope.isEditing && !$scope.isCreating;
  }

  $scope.shouldBeDefault = shouldBeDefault;
  $scope.startEditing = startEditing;
  $scope.startCreating = startCreating;
  $scope.cancelCreating = cancelCreating;
  $scope.cancelEditing = cancelEditing;
  $scope.shouldBeCreating = shouldBeCreating;
  $scope.shouldBeEditing = shouldBeEditing;

  
  // RESET FORM FIELDs

  function resetCreateForm() {
    $scope.newBusinessCard = {
    	logoSource: '',
      yourName: '',
      yourEmail: '',
      yourPhone: '',
      yourWebsite: ''
    }
  }

  // SUBMITTING NEW BUSINESS CARD DETAILS

  function createBusinessCard(businessCard) {
    businessCard.id = $scope.businessCards.length; // Generates an id for this card
    $scope.businessCards.push(businessCard);

    resetCreateForm();
    cancelCreating();
    
  }

  $scope.createBusinessCard = createBusinessCard;


  // UPDATING BUSINESS CARD

  $scope.editedBusinessCard = null;

  function setEditedBusinessCard(businessCard) {
      $scope.editedBusinessCard = angular.copy(businessCard);
  }

  $scope.setEditedBusinessCard = setEditedBusinessCard;

  function updateBusinessCard(businessCard) {
    var index = _.findIndex($scope.businessCards, function(b){
      return b.id == businessCard.id;
    });
    $scope.businessCards[index] = businessCard;
    $scope.editedBusinessCard = null;
    $scope.isEditing = false;
  }

  $scope.updateBusinessCard = updateBusinessCard;

  function isSelectedBusinessCard(businessCardId) {
    return $scope.editedBusinessCard !== null && $scope.editedBusinessCard.id === businessCardId;
  }

  $scope.isSelectedBusinessCard = isSelectedBusinessCard;

  // DELETE BUSINESS CARD

  function deleteBusinessCard(businessCard) {
    _.remove($scope.businessCards, function(b) {
      return b.id == businessCard.id;
    });
  }

  $scope.deleteBusinessCard = deleteBusinessCard;


});


