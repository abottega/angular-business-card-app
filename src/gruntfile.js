module.exports = function(grunt) {

    // Config
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Concatenate js files
        concat: {   
            dist: {
                src: [
                    'js/*.js', // Find all js files in the js folder
                ],
                dest: '../build/js/scripts.js',
            }
        },

        // Minify js
        uglify: {
            build: {
                src: '../build/js/scripts.js',
                dest: '../build/js/scripts.min.js'
            }
        },

        // Compile sass
        sass: {
            dist: {
                files: {
                    'css/styles.css' : 'css/styles.scss'
                }
            }
        },

        cssmin: {
            target: {
                files: [{
                    expand: true,
                    src: ['../build/css/styles.css'],
                    dest: '',
                    ext: '.min.css'
                }]
            }
        },

        autoprefixer: {
            options: {
                browsers: ['last 2 version', 'ie 9']
            },
            // prefix the specified file
            single_file: {
                src: 'css/styles.scss',
                dest: '../build/css/styles.css'
            }
        },

        copy: {
            html: {
                files: [{
                    expand: true,
                    src: '*.html',
                    dest: '../build/'
                }],
            },
            scripts: {
                files: [{
                    expand: true,
                    src: 'js/*.js',
                    dest: '../build/'
                }],
            },
            styles: {
                files: [{
                    expand: true,
                    src: 'css/*.css',
                    dest: '../build/'
                }],
            },
            images: {
                files: [{
                    expand: true,
                    src: 'img/*',
                    dest: '../build/'
                }],
            }
        },

        // Watch files
        watch: {
            scripts: {
                files: ['js/*.js'],
                tasks: ['concat', 'uglify'],
                options: {
                    spawn: false,
                },
            }, 
            css: {
                files: ['css/*.scss'],
                tasks: ['sass', 'autoprefixer'],
                options: {
                    spawn: false,
                }
            }
        }

    });

    // Load plugins
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');


    // Run tasks
    grunt.registerTask('default', ['concat', 'uglify', 'sass', 'cssmin', 'autoprefixer', 'copy']);

};